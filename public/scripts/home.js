$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function checkStatusUpdate(id) {
   
    console.log(id);
    $.ajax({
        url: '/update/' + id,
        type: 'put',

        success: function (response) {
            alert(response);
        },
        error: function (e) {
            console.log("ERROR ");
        },
    });
}
