<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" />
    <style>
        body {
            background-color: linen;
        }

        .row {
            padding: 10px 5px 10px 10px;
            border: 1px solid black;
            text-align: left;
            color: aliceblue;
            font-:
        }

        #body {
            background-color: rgb(21, 158, 182);
            width: 50%;
            text-align: center;
        }

        .alert-danger {
            color: red;
        }

        .alert-success {
            color: greenyellow;
        }

    </style>
</head>

<body>
    <div id="body">

        <h1>Work To-Dos</h1>
        <?php if(session('notification')): ?>
            <div class="alert-success">
                <?php echo e(session('notification')); ?>

            </div>
        <?php endif; ?>

        <?php if(count($errors) > 0): ?>
            <div class="alert-danger">
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $err): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php echo e($err); ?><br>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        <?php endif; ?>

        <form role="form" action="create" method="POST">
            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
            <input placeholder="New item..." name="name" type="text"> <button type="submit" class="btn">
                <img width="10px" height="10px" src=<?php echo e(asset('/icon/pencil.png')); ?> />
            </button>
        </form>
        <?php $__currentLoopData = $workToDos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $workToDo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <p class="row"><b><?php echo e($workToDo->name); ?></b>
                <?php if($workToDo->complete == 1): ?>
                    <input onclick="checkStatusUpdate(<?php echo e($workToDo->id); ?>)" type="checkbox" name="complete"
                        value="<?php echo e($workToDo->complete); ?>" checked>
                <?php else: ?>
                    <input onclick="checkStatusUpdate(<?php echo e($workToDo->id); ?>)" name="complete" type="checkbox"
                        value="<?php echo e($workToDo->complete); ?>">
                <?php endif; ?>

                <a href="<?php echo e(url('delete', $workToDo->id)); ?>"
                    onclick="return confirm('Do you really want to delete ?')">
                    <i style="font-size: 20px; color: red;" class="ti-trash">X</i>
                </a>
            </p>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
</body>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="<?php echo e(asset('/scripts/home.js')); ?>"></script>

</html>
<?php /**PATH /opt/lampp/htdocs/test/worktodo/resources/views/home.blade.php ENDPATH**/ ?>