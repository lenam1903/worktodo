<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <style>
        body {
            background-color: linen;
        }

        .row {
            padding: 10px 5px 10px 10px;
            border: 1px solid black;
            text-align: left;
            color: aliceblue;
            font-:
        }

        #body {
            background-color: rgb(21, 158, 182);
            width: 50%;
            text-align: center;
        }

        .alert-danger {
            color: red;
        }

        .alert-success {
            color: greenyellow;
        }

    </style>
</head>

<body>
    <div id="body">

        <h1>Work To-Dos</h1>
        @if (session('notification'))
            <div class="alert-success">
                {{ session('notification') }}
            </div>
        @endif

        @if (count($errors) > 0)
            <div class="alert-danger">
                @foreach ($errors->all() as $err)
                    {{ $err }}<br>
                @endforeach
            </div>
        @endif

        <form role="form" action="create" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input placeholder="New item..." name="name" type="text"> <button type="submit" class="btn">
                <img width="10px" height="10px" src={{ asset('/icon/pencil.png') }} />
            </button>
        </form>
        @foreach ($workToDos as $workToDo)
            <p class="row"><b>{{ $workToDo->name }}</b>
                @if ($workToDo->complete == 1)
                    <input onclick="checkStatusUpdate({{ $workToDo->id }})" type="checkbox" name="complete"
                        value="{{ $workToDo->complete }}" checked>
                @else
                    <input onclick="checkStatusUpdate({{ $workToDo->id }})" name="complete" type="checkbox"
                        value="{{ $workToDo->complete }}">
                @endif

                <a href="{{ url('delete', $workToDo->id) }}"
                    onclick="return confirm('Do you really want to delete ?')">
                    <i style="font-size: 20px; color: red;" class="ti-trash">X</i>
                </a>
            </p>
        @endforeach
    </div>
</body>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="{{ asset('/scripts/home.js') }}"></script>

</html>
