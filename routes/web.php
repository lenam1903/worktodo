<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WorkToDoController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [WorkToDoController::class, 'list']);
Route::post('/create', [WorkToDoController::class, 'create']);
Route::put('/update/{id}', [WorkToDoController::class, 'update']);
Route::get('/delete/{id}', [WorkToDoController::class, 'delete']);
