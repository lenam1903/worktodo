<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WorkToDo extends Model
{
    use HasFactory;

    protected $table = "work_to_dos";

    protected $fillable = [
        'name',
        'complete',
    ];
}
