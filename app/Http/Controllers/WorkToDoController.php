<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRequest;
use App\Models\WorkToDo;
use Illuminate\Http\Request;

class WorkToDoController extends Controller
{
    public function  list()
    {
        $workToDos = WorkToDo::orderBy('name', 'ASC')->get();
        return view('home', compact('workToDos'));
    }

    public function create(CreateRequest $request)
    {
        WorkToDo::create(['name' => $request->name]);
        return redirect('/')->with('notification', 'Created item successfully');
    }

    public function update($id)
    {
        try {
            $workToDo = WorkToDo::findOrFail($id);

            if ($workToDo->complete == 1) {
                $complete = 0;
                $message = "Not Complete";
            } else {
                $complete = 1;
                $message = "Completed";
            }
            $workToDo->update(['complete' => $complete]);
            return $message;
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function delete($id)
    {
        $workToDo = WorkToDo::findOrFail($id);
        $workToDo->delete();
        return redirect('/')->with('notification', 'Deleted item successfully');
    }
}
